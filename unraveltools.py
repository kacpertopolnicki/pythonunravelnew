#!/usr/bin/env python3

import os
import numpy as np
import argparse

def loadData(outdir):
    """
    Loads the arrays containing the simulation data in to arrays.

    string -> 
            { 
              "geometryIds" : <numpy array with event id and particle id for geometries> , 
              "geometries" : <numpy array with geometry data> , 
              "hitIds" : <numpy array with event id and particle id for hits> , 
              "hits" : <numpy array with hits>
            }
    """
    gid = np.load(os.path.join(outdir , "geometry_id.npy"))
    g = np.load(os.path.join(outdir , "geometry.npy"))
    hid = np.load(os.path.join(outdir , "hits_id.npy"))
    h = np.load(os.path.join(outdir , "hits.npy"))

    return {"geometryIds" : gid , "geometries" : g , "hitIds" : hid , "hits" : h}

def extractRandomChargedTrack(data , eventId = None , minHits = None):
    """
    Extracts a single charged track from the data.

        { 
          ... ,
          "geometryIds" : <numpy array with event id and particle id for geometries> , 
          "geometries" : <numpy array with geometry data> , 
          "hitIds" : <numpy array with event id and particle id for hits> , 
          "hits" : <numpy array with hits> ,
          ...
        } -> 
        { "geometry" : <numpy array with the geometry for chosen random track> ,
          "hits" : <numpy array with the hits>
        }

    """
    gid , g , hid , h = data["geometryIds"] , data["geometries"] , data["hitIds"] , data["hits"]

    where = None

    con = True
    while con:
        row = np.random.randint(g.shape[0])
         
        con = g[row , 0] == 0
        if(con):
            continue
        if(eventId is not None):
            con = con or gid[row , 0] != eventId
        if(con):
            continue
        if(minHits is not None):
            where = np.where((hid[: , 0] == gid[row , 0]) & (hid[: , 1] == gid[row , 1]))[0]
            con = con or where.shape[0] < minHits

    if(minHits is None):
        where = np.where((hid[: , 0] == gid[row , 0]) & (hid[: , 1] == gid[row , 1]))[0]

    return {"geometry" : g[row] , "hits" : h[where]} # , "hitsid" : hid[where]}

def extractEvent(data , eventId):
    """
    Extracts a single event.

        { 
          ... ,
          "geometryIds" : <numpy array with event id and particle id for geometries> , 
          "geometries" : <numpy array with geometry data> , 
          "hitIds" : <numpy array with event id and particle id for hits> , 
          "hits" : <numpy array with hits> ,
          ...
        } , <event number> -> 
        { "geometry" : <numpy array with the geometry for chosen random track> ,
          "hits" : <numpy array with the hits>
        }

    """
    gid , g , hid , h = data["geometryIds"] , data["geometries"] , data["hitIds"] , data["hits"]

    where = np.where(hid[: , 0] == eventId) 
    
    return {"hits" : h[where]}

def _unravelWithParameters(rx , ry , nu , zbar , hits):
    """
    Performs unraveling

    rx , ry , nu , zbar , <numpy array with hits> -> <numpy array with unraveled hits> 
    """
    helixCenter = np.array([rx , ry])
    theta = -nu * (hits[: , 2] - zbar) / np.linalg.norm(hits[: , [0 , 1]] - helixCenter , axis = 1)
    rotN1 = np.array([np.cos(theta), -np.sin(theta), np.sin(theta), np.cos(theta)])
    rotN = np.transpose(rotN1).reshape((theta.shape[0] , 2 , 2))
    xyN = helixCenter + np.einsum('ijk,ik->ij' , rotN , hits[:, [0, 1]] - helixCenter)

    return np.append(xyN , hits[: , [2]] , axis = 1)

def unravelFromTrajectory(zbar , traj , data):
    """
    zbar , 
    {... , "geometry" : <numpy array with track geometry> , ...} , 
    {... , "hits" : <numpy array with hits>} ->
    {"unraveledHits" : <numpy array with unraveled hits>}
    """
    rx , ry , nu = traj["geometry"][2:5]
    return {"unraveledHits" : _unravelWithParameters(rx , ry , nu , zbar , data["hits"])}

def _changeUnraveledCoordinates(rx , ry , unraveledhits):
    x = unraveledhits["unraveledHits"][: , 0]
    y = unraveledhits["unraveledHits"][: , 1]
    angles = np.arctan2(y - ry , x - rx) # see definition of arctan2
    distances = np.sqrt((x - rx) * (x - rx) + (y - ry) * (y - ry))
    travel = distances * angles

    return np.transpose(
            np.concatenate(
                [
                    distances.reshape((1 , distances.shape[0])) , 
                    travel.reshape((1 , travel.shape[0]))
                ] , axis = 0
                )
            )

def _quad(c):
    """
    Splits region c into four.

    <region> -> [<region> , <region> , <region> , <region>]
    """
    xMin , xMax , yMin , yMax = c
    return [
        [xMin , xMin + 0.5 * (xMax - xMin) , yMin , yMin + 0.5 * (yMax - yMin)] ,
        [xMin , xMin + 0.5 * (xMax - xMin) , yMin + 0.5 * (yMax - yMin) , yMax] ,
        [xMin + 0.5 * (xMax - xMin) , xMax , yMin , yMin + 0.5 * (yMax - yMin)] ,
        [xMin + 0.5 * (xMax - xMin) , xMax , yMin + 0.5 * (yMax - yMin) , yMax] 
        ]

def _count(data , c):
    """
    Counts the number of hits in region.

    {<numpy array with unraveled hits with transformed coordinates>} , <region> -> int
    """
    xMin , xMax , yMin , yMax = c
    cond = (data[: , 0] >= xMin) & (data[: , 0] < yMax) & (data[: , 1] >= yMin) & (data[: , 1] < yMax)
    return np.count_nonzero(cond)

def _iterate(data , thr , candidates):
    """

    """
    l = list(map(_quad , candidates))
    l = [val for sublist in l for val in sublist]
    result = []
    for c in l:
        n = _count(data , c)
        if(n > thr):
            result.append(c)
    return result

def sortBins(dx , dy , dz , data , verbose = False):
    """

    """
    hits = data['hits']

    smallx , largex = hits[: , 0].min() , hits[: , 0].max()
    smally , largey = hits[: , 1].min() , hits[: , 1].max()
    smallz , largez = hits[: , 2].min() , hits[: , 2].max()
    
    xval = np.floor(hits[: , 0] / dx)
    xmin , xmax = xval.min() , xval.max()
    xval = xval - xmin  

    yval = np.floor(hits[: , 1] / dy)
    ymin , ymax = yval.min() , yval.max()
    yval = yval - ymin

    zval = np.floor(hits[: , 2] / dz)
    zmin , zmax = zval.min() , zval.max()
    zval = zval - zmin

    ind = (
            zval + 
            yval * (zmax - zmin) + 
            xval * (zmax - zmin) * (ymax - ymin)
        ).reshape((hits.shape[0] , 1))
    
    joined = np.hstack((hits , ind))
    
    a = np.argsort(joined[: , 3])
    s = joined[a]

    bins = np.zeros(
            (
                int(xmax - xmin) , 
                int(ymax - ymin) , 
                int(zmax - zmin) ,
                2
            ) , dtype = np.uint64) - 1
    
    maxinbin = 0

    ii = 0
    for ix , iy , iz in np.ndindex(bins.shape[:3]):
        start = np.searchsorted(
                    s[: , 3] , 
                    iz + iy * (zmax - zmin) + ix * (zmax - zmin) * (ymax - ymin) ,
                    side = 'left'
                )   
        end = np.searchsorted(
                    s[: , 3] , 
                    iz + iy * (zmax - zmin) + ix * (zmax - zmin) * (ymax - ymin) ,
                    side = 'right'
                )     
        bins[ix , iy , iz , 0] = start
        bins[ix , iy , iz , 1] = end

        if(end - start > maxinbin):
            maxinbin = end - start

        if(verbose and ii % 100 == 0):
            print(str(int(0.5 + 100.0 * ii / ((xmax - xmin) * (ymax - ymin) * (zmax - zmin)))) + "%" , end = '\r')

        #if(end - start == 5):
        #    print(ix , iy , iz , end - start)
        ii += 1

    if(verbose):
        print('    ')
        print('maxinbin : ' , maxinbin)
    return {
                'dx' : dx ,
                'dy' : dy ,
                'dz' : dz ,
                'smallx' : smallx ,
                'smally' : smally ,
                'smallz' : smallz ,
                'largex' : largex ,
                'largey' : largey ,
                'largez' : largez ,
                'xmin' : xmin , 
                'xmax' : xmax , 
                'ymin' : ymin , 
                'ymax' : ymax , 
                'zmin' : zmin , 
                'zmax' : zmax ,
                'bins' : bins , 
                'sortedHits' : s[: , 0:3]
        }

def _getHits(rx , ry , nu , zbar , x , y , mul , bins):

    zz = np.linspace(bins['smallz'] , bins['largez'] , mul * int(bins['zmax'] - bins['zmin']))
    size = zz.shape[0]
    xx = x * np.ones(size)
    yy = y * np.ones(size)

    xx = xx.reshape((size , 1))
    yy = yy.reshape((size , 1))
    zz = zz.reshape((size , 1))

    streight = np.hstack((xx , yy , zz))
    
    helix = _unravelWithParameters(rx , ry , -nu , zbar , streight)

    ix = np.floor(helix[: , 0] / bins['dx']) - bins['xmin']
    iy = np.floor(helix[: , 1] / bins['dy']) - bins['ymin']
    iz = np.floor(helix[: , 2] / bins['dz']) - bins['zmin']

    ok = np.argwhere(
                (ix >= 0) & (ix < bins['xmax'] - bins['xmin']) &
                (iy >= 0) & (iy < bins['ymax'] - bins['ymin']) &
                (iz >= 0) & (iz < bins['zmax'] - bins['zmin'])
            ).flatten()

    ix = ix.reshape((size , 1))
    iy = iy.reshape((size , 1))
    iz = iz.reshape((size , 1))
    
    binPositions = np.hstack((ix , iy , iz))[ok].astype(np.uint64)

    binInd = bins['bins'][binPositions[: , 0] , binPositions[: , 1] , binPositions[: , 2]]
    binInd = binInd[np.argwhere(binInd[: , 0] != binInd[: , 1]).flatten()]

    binIndUnq = np.unique(binInd , axis = 0) # slow

    return binIndUnq
    
def _getPoints(sortedHits , ranges):
    return np.concatenate([sortedHits[r[0] : r[1]] for r in ranges])

def _getHitsI(ix , iy , iz , bins):
    start = bins['bins'][int(ix) , int(iy) , int(iz) , 0]
    end = bins['bins'][int(ix) , int(iy) , int(iz) , 1]
    return bins['sortedHits'][start : end]

def _getHitsV(x , y , z , bins):
    ix = np.floor(x / bins['dx']) - bins['xmin']
    iy = np.floor(y / bins['dy']) - bins['ymin']
    iz = np.floor(z / bins['dz']) - bins['zmin']
    start = bins['bins'][int(ix) , int(iy) , int(iz) , 0]
    end = bins['bins'][int(ix) , int(iy) , int(iz) , 1]
    return bins['sortedHits'][start : end]

if(__name__ == "__main__"):
    parser = argparse.ArgumentParser(description = "Test unraveltools module.")
    parser.add_argument("input" , help = "Directory with the input data.")
    parser.add_argument("num" , help = "Number of iterations.")

    args = parser.parse_args()
    
    data = loadData(args.input)
    t = extractRandomChargedTrack(data , eventId = 10 , minHits = 10)
    event = extractEvent(data , 10)
    print('t["hits"][9 , 2] : ' , t["hits"][9 , 2])
    unr = unravelFromTrajectory(t["hits"][9 , 2] , t , event)

    rx , ry , nu = t["geometry"][2:5]
    print("rx ry nu : " , rx , ry , nu)

    unrAfterChange = _changeUnraveledCoordinates(rx , ry , unr)

    xMin , xMax = unrAfterChange[: , 0].min() , unrAfterChange[: , 0].max()
    yMin , yMax = unrAfterChange[: , 1].min() , unrAfterChange[: , 1].max()

    candidates = [[xMin , xMax , yMin , yMax]]
    print("candidates : " , candidates)

    for i in range(int(args.num)):
        candidates = _iterate(unrAfterChange , 5 , candidates)
        print("i : " , i)
        print("len(candidates) : " , len(candidates))




