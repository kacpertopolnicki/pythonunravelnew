#!/usr/bin/env python3

import numpy as np
from unraveltools import *
from unraveltools import _getHits
from unraveltools import _getPoints

data = loadData("out_temp_1")
event = extractEvent(data , 10)
bins = sortBins(0.01 , 0.01 , 0.01 , event , verbose = True)

print(bins["xmin"] , bins["ymin"] , bins["zmin"])
print(bins["xmax"] , bins["ymax"] , bins["zmax"])

for i in range(100):
    print("#  " , i)
    
    #print("##  chosing random geometry")
    
    t = extractRandomChargedTrack(data , eventId = 10 , minHits = 6)
    rx , ry , nu = t["geometry"][2:5]
    
    #print("##  rx , ry , nu : " , rx , ry , nu)
    #print("##  t['hits'][3] : " , t["hits"][3])
    #print("##  geting ranges in original hits that unravel to geometry")
    
    binInd = _getHits(
                        rx , ry , nu , 
                        t["hits"][3 , 2] , t["hits"][3 , 0] , t["hits"][3 , 1] , 
                        10 , bins
                    )
    
    #print("##  getting hits")
    
    pts = _getPoints(bins["sortedHits"] , binInd)
    np.save("trajHits_" + str(i) + ".npy" , t["hits"])
    np.save("trajGeom_" + str(i) + ".npy" , t["geometry"])
    
    print(pts.shape , t["hits"].shape)
    #print("##  checking hits")
    
    chk = ""
    for el in t["hits"]:
        d = np.linalg.norm(pts - el , axis = 1)
        d.sort()
        print("##  should be close to 0 : " , d[0] , el)
        chk += "##  should be close to 0 : " + str(el) + str(d[0]) + "\n"
    with open("trajCheck_" + str(i) + ".npy" , "w") as f:
        f.write(chk)


