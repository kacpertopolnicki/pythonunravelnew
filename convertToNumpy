#!/usr/bin/env python

import sys
import os
import argparse
import ROOT
import numpy as np
import time

if(__name__ == "__main__"):

    # parsing arguments
    parser = argparse.ArgumentParser(description = "Convert ROOT .root files into numpy .npy files.")
    parser.add_argument("output" , help = "Path to output directory.")
    parser.add_argument("--geometryFile" , "-g" , help = "Path to .root file with the geometry.")
    parser.add_argument("--hitsFile" , "-t" , help = "Path to .root file with hits.")
    parser.add_argument("--verbose" , "-v" , action = "store_true" , help = "Verbose output.")
    args = parser.parse_args()

    if(os.path.exists(args.output)):
        sys.stderr.write("File / directory " + args.output + " exists, exiting.\n")
        sys.exit(1)

    os.mkdir(args.output)

    if(args.geometryFile is not None):

        # writing file with information about the geometries

        with open(os.path.join(args.output , "GEOMETRY_README") , "w") as f:
            f.write("Each row of a geometry.npy file contains:\n")
            f.write("valid - TODO\n")
            f.write("helix radius - TODO\n")
            f.write("helix center x - TODO\n")
            f.write("helix center y - TODO\n")
            f.write("nu - TODO\n")
            f.write("vx , vy , vz - TODO\n")
            f.write("Each row of a geometry_id.npy file contains:\n")
            f.write("event_id - TODO\n")
            f.write("particle_id - a unique number for the geometry\n")

        # root file containing the geometry of each particle track

        geometryFile = ROOT.TFile(args.geometryFile , "READ") 
        geometryTree = geometryFile.Get("particles")

        # reading particle geometries

        if(args.verbose):
            sys.stderr.write("READING PARTICLE GEOMETRIES ... \n")
            sys.stderr.flush()

        allGeom = []
        allGeomid = []

        for event in geometryTree:
            event_id , particle_id , vx , vy , vz , px , py , pz , eta , phi , pt , mass , charge = (
                    event.event_id,
                    event.particle_id,
                    event.vx,
                    event.vy,
                    event.vz,
                    event.px,
                    event.py,
                    event.pz,
                    event.eta,
                    event.phi,
                    event.pt,
                    event.m,
                    event.q
            )                

            nofParticles = len(particle_id)

            geom = np.zeros((nofParticles , 8) , dtype = np.float64)
            geom1 = np.zeros((nofParticles , 2) , dtype = np.uint)
            
            for particle in range(nofParticles):
                valid = 0.0 
                helixRadius = 0.0
                helixCenter = np.array([0.0 , 0.0]) 
                nu = 0.0
                if(charge[particle] != 0):
                    valid = 1.0
                    helixRadius = 3.33564 * (pt[particle] / np.abs(charge[particle] * (-2.0)))
                    helixPitch = 3.33564 * (pz[particle] * 2.0 * np.pi / (charge[particle] * (-2.0)))
                    toCenter = -np.sign(charge[particle]) * np.array([-py[particle] , px[particle]]) 
                    toCenter = toCenter / np.sqrt(np.dot(toCenter , toCenter))
                    helixCenter = np.array([vx[particle] / 1000.0 , vy[particle] / 1000.0]) + helixRadius * toCenter
                    nu = 2.0 * np.pi * helixRadius / helixPitch


                geom[particle] = [
                                    valid,
                                    helixRadius , 
                                    helixCenter[0] , 
                                    helixCenter[1] , 
                                    nu ,
                                    vx[particle] / 1000.0 , 
                                    vy[particle] / 1000.0 , 
                                    vz[particle] / 1000.0
                                ]
                geom1[particle] = [
                                    event_id ,
                                    particle_id[particle] ,
                                ]
            
            allGeom.append(geom)
            allGeomid.append(geom1)
        
        name = os.path.join(args.output , "geometry" + ".npy")
        name1 = os.path.join(args.output , "geometry_id" + ".npy")
        
        if(args.verbose):
            sys.stderr.write("SAVING GEOMETRY DATA TO : " + name + "\n")
            sys.stderr.write("SAVING GEOMETRY ID TO : " + name1 + "\n")
        
        np.save(name , np.concatenate(allGeom))
        np.save(name1 , np.concatenate(allGeomid))
                
    if(args.hitsFile is not None):

        # information about the hits 

        with open(os.path.join(args.output , "HITS_README") , "w") as f:
            f.write("Each row of a hits_....npy file contains:\n")
            f.write("tx , ty , tz - TODO\n")
            f.write("Each row of a hits_id_....npy file contains:\n")
            f.write("event_id - TODO\n")
            f.write("particle_id - a unique number for the geometry\n")
        
        # root file containing the hits

        hitsFile = ROOT.TFile(args.hitsFile , "READ") 
        hitsTree = hitsFile.Get("hits")

        # reading data from hits

        if(args.verbose):
            sys.stderr.write("READING DATA FROM HITS ...\n")
            sys.stderr.flush()
        
        nofHits = hitsTree.GetEntries()

        hitsData = np.zeros((nofHits , 3) , dtype = np.float64)
        hitsId = np.zeros((nofHits , 2) , dtype = np.uint)
        
        name = os.path.join(args.output , "hits" + ".npy")
        name1 = os.path.join(args.output , "hits_id" + ".npy")

        ii = 0
        for event in hitsTree:
                tx , ty , tz , event_id , particle_id = event.tx , event.ty , event.tz , event.event_id , event.particle_id
                hitsData[ii , 0] = tx / 1000.0            
                hitsData[ii , 1] = ty / 1000.0            
                hitsData[ii , 2] = tz / 1000.0            
                hitsId[ii , 0] = event_id            
                hitsId[ii , 1] = particle_id
                ii += 1
        
        if(args.verbose):
            sys.stderr.write("READ " + str(hitsData.shape[0]) + " hits in D.\n")

        if(args.verbose):
            sys.stderr.write("SAVING HITS DATA TO : " + name + "\n")
            sys.stderr.write("SAVING HITS ID TO : " + name1 + "\n")

        np.save(name , hitsData)
        np.save(name1 , hitsId)
