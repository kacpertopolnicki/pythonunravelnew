def sort(data , dx , dy , dz):
    hits = data['hits']
    
    xval = np.floor(hits[: , 0] / dx)
    xmin , xmax = xval.min() , xval.max()
    xval = xval - xmin  

    yval = np.floor(hits[: , 1] / dy)
    ymin , ymax = yval.min() , yval.max()
    yval = yval - ymin

    zval = np.floor(hits[: , 2] / dz)
    zmin , zmax = zval.min() , zval.max()
    zval = zval - zmin

    bins = np.zeros((int(xmax - xmin) , int(ymax - ymin) , int(zmax - zmin)) , dtype = np.uint64) - 1

    ind = (zval + yval * (zmax - zmin) + xval * (zmax - zmin) * (ymax - ymin)).reshape((hits.shape[0] , 1))
    
    joined = np.hstack((hits , ind))
    
    a = np.argsort(joined[: , 3])
    s = joined[a][:]

    for ix , iy , iz in np.ndindex(bins.shape):
        start = np.searchsorted(s[: , 3] , iz + iy * (zmax - zmin) + ix * (zmax - zmin) * (ymax - ymin))    
        bins[ix , iy , iz] = start

    print(bins)
    
    return {'xbins' : xmax - xmin , 'ybins' : ymax - ymin , 'zbins' : zmax - zmin , 'bins' : bins , 'sortedHits' : s[: , 0:3]}
